﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.IO;
using System.Linq;
using System.Net;
using System;
using Facebook.Unity;

public class GameManager : MonoBehaviour
{
    private ApiManager apiManager;
    public GameObject panelError;
    public GameObject panelLoading;
	public ProfileManager	profileManager;
	public Text debugteks;
	private string email;


    private void Awake()
    {
        if (!FB.IsInitialized)
        {
            FB.Init(InitCallback, OnHideUnity);
        }
        else
       	{
            FB.ActivateApp();

        }

		if (profileManager	!=	null) {
			profileManager.StreamProfile ();
		}
    }

    private void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();
            // Continue with Facebook SDK
            // ...
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
         //   Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
           // Time.timeScale = 1;
        }
    }

    #region Login

    public void BtnLogin()
    {
        email = GameObject.Find("username").GetComponent<InputField>().text;
        string password = GameObject.Find("password").GetComponent<InputField>().text;
        GetApiManager().Login(email, password);
        panelLoading.SetActive(true);
    }

    public void BtnRegister()
    {

    }

    public void LoginSuccess(bool isLoginFB)
    {
		PlayerPrefs.SetInt("telah-login", 1);
        print("success");
        
        panelLoading.SetActive(false);
		SetLocalEmail (email);

        if (isLoginFB)
        {
            PlayerPrefs.SetString("login", "fb");
        }
        else
        {
            PlayerPrefs.SetString("login", "notfb");
        }
		ChangeScene(3);
    }

    public void LoginFailed()
    {
        print("failed");
        panelError.SetActive(true);
        panelLoading.SetActive(false);
        Invoke("HidePanelDataSalah", 2);
    }

    #endregion Login

    #region Register

    public void R_BtnLogin()
    {

    }

    public void R_BtnRegister()
    {
        string new_email = GameObject.Find("R_email").GetComponent<InputField>().text;
        string username = GameObject.Find("R_username").GetComponent<InputField>().text;
        string password = GameObject.Find("R_password").GetComponent<InputField>().text;
        string confirmPassword = GameObject.Find("R_confirm password").GetComponent<InputField>().text;
        GetApiManager().Register(new_email, username, password, confirmPassword);
        email = new_email;
        panelLoading.SetActive(true);
    }

    public void RegisterSuccess()
    {
        print("success");
        ChangeScene(3);
        panelLoading.SetActive(false);
		SetLocalEmail (email);
    }

    public void RegisterFailed()
    {
        print("failed");
        panelError.SetActive(true);
        panelLoading.SetActive(false);
        Invoke("HidePanelDataSalah", 2);
    }

    #endregion Register

    public void LoginWithFacebook()
    {
        panelLoading.SetActive(true);
        var perms = new List<string>() { "public_profile", "email", "user_friends" };
        FB.LogInWithReadPermissions(perms, AuthCallback);
    }

    private void AuthCallback(ILoginResult result)
    {
        panelLoading.SetActive(false);
        if (FB.IsLoggedIn)
        {
            var aToken = Facebook.Unity.AccessToken.CurrentAccessToken;
            Debug.Log(aToken.UserId);
            foreach (string perm in aToken.Permissions)
            {
                //print(perm);
                
            }
            FetchFBProfile();
        }
        else
        {
            Debug.Log("User cancelled login");
        }
    }

    private void FetchFBProfile()
    {
        panelLoading.SetActive(true);
        FB.API("/me?fields=first_name,last_name,email", HttpMethod.GET, FetchProfileCallback, new Dictionary<string, string>() { });
    }

    private void FetchProfileCallback(IGraphResult result)
    {
        var FBUserDetails = (Dictionary<string, object>)result.ResultDictionary;
        //Debug.Log("Profile: first name: " + FBUserDetails["first_name"]);
        //Debug.Log("Profile: last name: " + FBUserDetails["last_name"]);
        //Debug.Log("Profile: id: " + FBUserDetails["id"]);
        //Debug.Log("Profile: email: " + FBUserDetails["email"]);
        //debugteks.text = FBUserDetails["email"].ToString();
		
        GetApiManager().LoginFB(FBUserDetails["first_name"].ToString(), FBUserDetails["first_name"].ToString());
        email = FBUserDetails["first_name"].ToString();
    }

    private ApiManager GetApiManager()
    {
        apiManager = GameObject.Find("APIManager").GetComponent<ApiManager>();
        return apiManager;
    }

    public void ChangeScene(int sceneIndex)
    {
        SceneManager.LoadScene(sceneIndex);
    }

	public void SetPreferencesTutorial(string _value)
	{
		string prefsName = "isHaveLikeFB";
		PlayerPrefs.SetString (prefsName, _value);
	}

	public string GetPreferencesTutorial()
	{
		return PlayerPrefs.GetString ("isHaveLikeFB");
	}

    private void HidePanelDataSalah() {
        panelError.SetActive(false);
    }

	private void SetLocalEmail(string email){
		PlayerPrefs.SetString ("email", email);
	}

	public	void	Logout(){
		FB.LogOut ();
		PlayerPrefs.DeleteAll ();
		ChangeScene (1);
	}
}


