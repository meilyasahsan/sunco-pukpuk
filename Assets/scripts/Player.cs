﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private RuntimePlatform platform = Application.platform;
    private GameManager gameManager;
    private GamePlay gamePlay;
	public Sprite[] spritePerLevel;
	private SpriteRenderer spriteRenderer;
    private Animator playerAnimator;
    private SoundManager soundManager;
    private AudioSource soundScoring;

    private void Start()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        gamePlay = GameObject.Find("Main Camera").GetComponent<GamePlay>();
        soundManager = GameObject.Find("SoundManager").GetComponent<SoundManager>();
        soundScoring = GameObject.Find("Sound Scoring").GetComponent<AudioSource>();
        playerAnimator = GetComponent<Animator>();
		spriteRenderer = GetComponent<SpriteRenderer>();
        StartCoroutine(ChangePosIfChangeLevel());
    }

    private void Update()
    {
        if(platform == RuntimePlatform.Android || platform == RuntimePlatform.IPhonePlayer)
        {
            if(Input.touchCount > 0)
            {
                if(Input.GetTouch(0).phase == TouchPhase.Began)
                {
                    CheckTouch(Input.GetTouch(0).position);
                }
            }
        }else if(platform == RuntimePlatform.WindowsEditor)
        {
            if (Input.GetMouseButtonDown(0))
            {
                CheckTouch(Input.mousePosition);
            }
        }
    }

    private void CheckTouch(Vector3 pos)
    {
        Vector3 wp = Camera.main.ScreenToWorldPoint(pos);
        Vector2 touchPos = new Vector2(wp.x, wp.y);
        Collider2D hit = Physics2D.OverlapPoint(touchPos);
        if (hit)
        {
            string playerName = hit.gameObject.tag;
            if(playerName == "Player")
            {
                Clicked();
                if (gamePlay.isPlay)
                {
                    soundManager.AudioTimer();
                    GameObject tapCLone = Instantiate(gamePlay.tap, new Vector3(wp.x, wp.y, 0), Quaternion.identity);
                    GameObject minyak = Instantiate(gamePlay.minyak, new Vector3(wp.x, wp.y, 0),Quaternion.identity);
                    Destroy(minyak, 2);
                    Destroy(tapCLone, 1);
                }
            }
        }
    }

    private void Clicked()
    {
        gamePlay.PlayerClicked();
        
    }

	public void ChangeSpriteRender(int level)
	{
        spriteRenderer.sprite = spritePerLevel[level];
        Destroy(GetComponent<PolygonCollider2D>());
        gameObject.AddComponent<PolygonCollider2D>();
	}

    public IEnumerator ChangePosIfChangeLevel()
    {

        GetComponent<SpriteRenderer>().sprite = spritePerLevel[gamePlay.levelNow];
        playerAnimator.SetBool("isChangeLevel", true);
        GetComponent<SpriteRenderer>().enabled = false;
        Invoke("EnableSR", 0.5f);
        yield return new WaitForSeconds(1);
        playerAnimator.SetBool("isChangeLevel", false);
    }

    private void EnableSR()
    {
        GetComponent<SpriteRenderer>().enabled = true;
    }
}
