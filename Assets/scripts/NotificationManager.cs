﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NotificationManager : MonoBehaviour {

    public GameObject[] notifs;

    public void DisplayNotif(bool isGood)
    {
        if (isGood)
        {
            notifs[0].SetActive(true);
            notifs[1].SetActive(false);
        }
        else
        {

            notifs[0].SetActive(false);
            notifs[1].SetActive(true);
        }
    }

    public void HideAll()
    {
        notifs[0].SetActive(false);
        notifs[1].SetActive(false);
    }
}
