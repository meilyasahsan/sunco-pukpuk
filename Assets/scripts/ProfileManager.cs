﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using Facebook.Unity;

public class ProfileManager : MonoBehaviour {

    public Text emailUser;
    private Texture2D profilePic;
    public RawImage rawProfileImage;

    private void Start()
    {
        try
        {
            emailUser.text = PlayerPrefs.GetString("email");
            if (PlayerPrefs.GetString("login") == "fb")
            {
                FB.API("/me/picture", HttpMethod.GET, this.ProfilePhotoCallback);
            }
        }
        catch(NullReferenceException ex)
        {

        }
    }

	public void StreamProfile(){
		try
		{
			emailUser.text = PlayerPrefs.GetString("email");
			if (PlayerPrefs.GetString("login") == "fb")
			{
				FB.API("/me/picture", HttpMethod.GET, this.ProfilePhotoCallback);
			}
		}
		catch(NullReferenceException ex)
		{

		}
	}

    private void ProfilePhotoCallback(IGraphResult result)
    {
        if (string.IsNullOrEmpty(result.Error) && result.Texture != null)
        {
            this.profilePic = result.Texture;
        }

        this.HandleResult(result);
    }

    private void HandleResult(IResult result)
    {
        if(result != null)
        {
            rawProfileImage.texture = profilePic;
        }
        else
        {

        }
    }
}
