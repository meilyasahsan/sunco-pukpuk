﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using SimpleJSON;
using UnityEngine.UI;

public class ApiManager : MonoBehaviour
{
    private string suncoapiURL = "http://asdasd.id/suncoapi";
    private string loginAPI = "/user/login";
	private string loginFBAPI = "/user/loginfb";
    private string registerAPI = "/user/register";
    private string updateScoreAPI = "/user/update-score";
    private string highscoreAPI = "/user/highscore";
    private GameManager gameManager;
 

    private GameManager getGameManager()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        return gameManager;
    }

    public void Login(string email, string password)
    {
        WWWForm form = new WWWForm();
        form.AddField("email", email);
        form.AddField("password", password);
        WWW www = new WWW(suncoapiURL + loginAPI, form);
        StartCoroutine(LoginRequest(www));
    }

    private IEnumerator LoginRequest(WWW www)
    {
        yield return www;
        var result = JSON.Parse(www.text);
        try
        {
            var status = result["status"];
            if(status == true)
            {
                getGameManager().LoginSuccess(false);
            }
            else
            {
                getGameManager().LoginFailed();
            }
               
        }catch(NullReferenceException ex)
        {
            getGameManager().LoginFailed();
        }
    }

	public void LoginFB(string email, string username){
		WWWForm form = new WWWForm();
		form.AddField("email", email);
		form.AddField("username", username);
		WWW www = new WWW(suncoapiURL + loginFBAPI, form);
		StartCoroutine(LoginFBRequest(www));
	}

	private IEnumerator LoginFBRequest(WWW www)
	{
		yield return www;
		var result = JSON.Parse(www.text);
		try
		{
			var status = result["status"];
			if(status == true)
			{
				getGameManager().LoginSuccess(true);
			}
			else
			{
				getGameManager().LoginFailed();
			}

		}catch(NullReferenceException ex)
		{
			getGameManager().LoginFailed();
		}
	}

    public void Register(string email, string username,string password, string confirmPassword)
    {
        WWWForm form = new WWWForm();
        form.AddField("email", email);
        form.AddField("username", username);
        form.AddField("password", password);
        form.AddField("confirm_password", confirmPassword);
        WWW www = new WWW(suncoapiURL + registerAPI, form);
        StartCoroutine(RegisterRequest(www));
    }

    private IEnumerator RegisterRequest(WWW www)
    {
        yield return www;
        var result = JSON.Parse(www.text);
        print(result);
        try
        {
            var status = result["status"];
            if (status == true)
            {
                getGameManager().RegisterSuccess();
            }
            else
            {
                getGameManager().RegisterFailed();
            }

        }
        catch (NullReferenceException ex)
        {
            getGameManager().RegisterFailed();
        }
    }

    public void UpdateScore()
    {

    }

    private IEnumerator UpdateScoreRequest(WWW www)
    {
        yield return www;
        var result = JSON.Parse(www.text);
        try
        {
            var status = result["status"];
            if (status == true)
            {

            }
            else
            {

            }

        }
        catch (NullReferenceException ex)
        {
            //failed
        }
    }

    public void Highscore()
    {

    }

    private IEnumerator HighscoreRequest(WWW www)
    {
        yield return www;
        var result = JSON.Parse(www.text);
        try
        {
            var status = result["status"];
            if (status == true)
            {

            }
            else
            {

            }

        }
        catch (NullReferenceException ex)
        {
            //failed
        }
    }
}
