﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScreenSaver : MonoBehaviour {

	// Use this for initialization
	IEnumerator Start () {
        yield return new WaitForSeconds(2);

		if (PlayerPrefs.GetInt ("telah-login") != 1) {
			SceneManager.LoadScene("login");

		} else {
			SceneManager.LoadScene("main menu");
		}
	}

}
