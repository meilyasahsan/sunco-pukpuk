﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class GamePlay : MonoBehaviour {

    public float[] levelTimes;
    public float currentTime = 0.0f;

    public bool isPlay = false;
    public bool isGameover = false;

    public int levelNow = 0;
    public int[] scoreLevels;
    public int score = 0;

    private GameManager gameManager;
    private LevelIndexManager levelIndexManager;

    private GameObject scoreDisplayer;
    private GameObject scoreDisplayerShadow;
    private GameObject timerDisplayer;
    private GameObject sliderTime;
	private GameObject player;
	private GameObject mangkuk;
    private GameObject namaObjek;
	public GameObject panelGameOver;
    public GameObject gameOverScore;
    public GameObject gameOverScoreShadow;
    public GameObject tap;
    public GameObject minyak;
    public GameObject[] objeckPukpuk;
    public AudioSource[] audioSources;
    public bool isHasTap = false;
    public NotificationManager notifManager;

	void Start ()
    {
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
        levelIndexManager = GameObject.Find("level indexer").GetComponent<LevelIndexManager>();
        scoreDisplayer = GameObject.Find("score displayer");
        scoreDisplayerShadow = GameObject.Find("score displayer shadow");
        timerDisplayer = GameObject.Find("timer displayer");
        sliderTime = GameObject.Find("Slider Time");
		player = GameObject.FindGameObjectWithTag ("Player");
		mangkuk = GameObject.Find ("mangkuk");
        namaObjek = GameObject.Find("nama objek");
        StartCoroutine(StartGame());
        InvokeRepeating("DisplayNotif", 5, 5);
    }

    private void Update()
    {
        if (isPlay && !isGameover)
        {
            currentTime -= Time.deltaTime;
            ManageSlider(levelTimes[levelNow], currentTime);
            int timeInInteger = (int)currentTime;
            timerDisplayer.GetComponent<Text>().text = timeInInteger.ToString();
            if(currentTime < 0)
            {
                currentTime = 0;
                BreakLevel();
            }
        }
    }

    private void DisplayNotif()
    {
        if (isGameover) return;
        if (isHasTap)
        {
            notifManager.DisplayNotif(true);
        }
        else
        {
            notifManager.DisplayNotif(false);
        }
        isHasTap = false;
        Invoke("HideAllNotif", 1);
    }

    private void HideAllNotif()
    {
        notifManager.HideAll();
    }

    private IEnumerator StartGame()
    {
        yield return new WaitForSeconds(1);
        levelNow = 0;
        sliderTime.GetComponent<Slider>().maxValue = levelTimes[levelNow];
        isPlay = true;
        currentTime = levelTimes[0];
    }

    private void BreakLevel()
    {
        isPlay = false;
		if (levelNow >= levelTimes.Length) {
			GameOver ();
		} else {
            float waktuJeda = 0.5f;
            switch (levelNow)
            {
                case 0:
                    HideWhenChangeLevel(false);
                    waktuJeda = 2;
                    GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>().enabled = false;
                    gameOverScore.GetComponent<Text>().text = score.ToString();
                    gameOverScoreShadow.GetComponent<Text>().text = score.ToString();
                    levelIndexManager.ChangeDisplayCentang(0);
                    break;
                case 2:
                    HideWhenChangeLevel(false);
                    waktuJeda = 2;
                    GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>().enabled = false;
                    gameOverScore.GetComponent<Text>().text = score.ToString();
                    gameOverScoreShadow.GetComponent<Text>().text = score.ToString();
                    levelIndexManager.ChangeDisplayCentang(1);
                    break;
                default:
                    
                    break;
            }
			StartCoroutine(CountinueLevel(waktuJeda));
		}
    }

    private void HideWhenChangeLevel(bool _value)
    {
        GameObject[] tapClones = GameObject.FindGameObjectsWithTag("tap");
        foreach(GameObject obj in tapClones)
        {
            Destroy(obj);
        }
        mangkuk.SetActive(_value);
        panelGameOver.SetActive(!_value);
    }

    private IEnumerator CountinueLevel(float time)
    {
       
        yield return new WaitForSeconds(time);
        GameObject.FindGameObjectWithTag("Player").GetComponent<SpriteRenderer>().enabled = true;

        if (levelNow >= levelTimes.Length - 1)
        {
            currentTime = 0;
			GameOver ();
        }
        else
        {
            levelNow++;
            sliderTime.GetComponent<Slider>().maxValue = levelTimes[levelNow];
            StartCoroutine(GameObject.FindGameObjectWithTag("Player").GetComponent<Player>().ChangePosIfChangeLevel());
            currentTime = levelTimes[levelNow];    
            isPlay = true;
            HideWhenChangeLevel(true);
            namaObjek.GetComponent<NamaObjekManager>().ChangeSprite(levelNow);
        }
    }

    public void PlayerClicked()
    {
        if(isPlay && !isGameover)
        {
            int scoreAddition = scoreLevels[levelNow];
            score = score + scoreAddition;
            scoreDisplayer.GetComponent<Text>().text = score.ToString();
            scoreDisplayerShadow.GetComponent<Text>().text = score.ToString();
            isHasTap = true;
        }
    }

    private void ManageSlider(float _maxValue, float _value)
    {
        Slider slider = sliderTime.GetComponent<Slider>();
        slider.maxValue = _maxValue;
        slider.value = _value;
    }

	private void GameOver()
	{
		player.SetActive (false);
		mangkuk.SetActive (false);
		panelGameOver.SetActive (true);
        gameOverScore.GetComponent<Text>().text = score.ToString();
        gameOverScoreShadow.GetComponent<Text>().text = score.ToString();
        panelGameOver.GetComponent<PanelScoreManager>().HideShowButton(panelGameOver.GetComponent<PanelScoreManager>().buttonGameover, true);
        levelIndexManager.ChangeDisplayCentang(2);
        GameOverSound();
    }

    private void GameOverSound()
    {
        audioSources[0].Stop();
        audioSources[1].Stop();
        audioSources[2].Stop();
        audioSources[3].Play();
    }
}
