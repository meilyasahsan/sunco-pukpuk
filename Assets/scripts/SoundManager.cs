﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour {

    public AudioClip audioCoin;
    public AudioClip audioTimer;
    public AudioClip audioTap;
    public AudioClip audioScoring;
    public AudioClip audioMinyak;
    public AudioClip audiogameover;
    public AudioClip audioButton;
    public Image btnSound;
    public Sprite[] btnSoundSprite;

    private void Awake()
    {
        if(PlayerPrefs.GetInt("isFirstTime") != 99)
        {
            PlayerPrefs.SetInt("isFirstTime", 99);
            PlayerPrefs.SetString("sound", "normal");
            AudioListener.volume = 1;
        }
    }

    private void Start()
    {
        if(PlayerPrefs.GetString("sound") == "normal")
        {
            AudioListener.volume = 1;
            if (btnSound == null) return;
            btnSound.sprite = btnSoundSprite[0];
        }
        else
        {
            AudioListener.volume = 0;
            if (btnSound == null) return;
            btnSound.sprite = btnSoundSprite[1];
        }
    }

    public void BtnSoundChange()
    {
        if (PlayerPrefs.GetString("sound") == "normal")
        {
            PlayerPrefs.SetString("sound", "mute");
            AudioListener.volume = 0;
            btnSound.sprite = btnSoundSprite[1];
        }
        else
        {
            PlayerPrefs.SetString("sound", "normal");
            AudioListener.volume = 1;
            btnSound.sprite = btnSoundSprite[0];
        }
    }

    public void AudioTimer()
    {
        GetComponent<AudioSource>().clip = audioTap;
        GetComponent<AudioSource>().Play();
    }

    public void AudioMinyak()
    {
        GetComponent<AudioSource>().clip = audioMinyak;
        GetComponent<AudioSource>().Play();
    }

    public void MakeSound(AudioClip audiox)
    {
        AudioSource.PlayClipAtPoint(audiox, transform.position);
    }
}
