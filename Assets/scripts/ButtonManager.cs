﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour {

    public Sprite[] spriteButtonSound;

	public void ChangeSpriteButtonSound(int indexButton)
    {
        GetComponent<Image>().sprite = spriteButtonSound[indexButton];
    }
}
