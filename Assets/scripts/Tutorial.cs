﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tutorial : MonoBehaviour {

    private GameManager gameManager;
	public GameObject panelLikeFB;

    private void Start()
    {
        
        gameManager = GameObject.Find("GameManager").GetComponent<GameManager>();
		if (gameManager.GetPreferencesTutorial () != "sudah") {
			StartCoroutine (GoToStart());
		} else {
			StartCoroutine (GoToStart());
		}
    }

    IEnumerator GoToStart ()
    {
        yield return new WaitForSeconds(3);
        gameManager.ChangeScene(3);
	}

	IEnumerator PanelLikeFB(){
		yield return new WaitForSeconds (3);
		panelLikeFB.SetActive (true);
	}

	public void LikeFB(){
		gameManager.SetPreferencesTutorial ("sudah");
		panelLikeFB.SetActive (false);
		gameManager.ChangeScene (2);
	}
}
