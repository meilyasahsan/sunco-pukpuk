﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NamaObjekManager : MonoBehaviour 
{
    public Texture[] images;

    public void ChangeSprite(int index)
    {
        GetComponent<RawImage>().texture = images[index];
    }
}
