﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;

public class ShareManager : MonoBehaviour {
    private string komikNgPlaystore = "facebook.com/SunCoIndonesia/";
    private string suncoURL = "https://minyakgorengsunco.com/";
    private bool isAndroid = false;
    public GameObject panelShareFanpage;

    public void shareMedsos(int indexSocmed)
    {
        int score = GameObject.Find("Main Camera").GetComponent<GamePlay>().score;
        string[] linkURL = new string[2] { "https://www.facebook.com/dialog/share?app_id=184484190795&display=popup&e2e=%7B%7D&href=https%3A%2F%2F" + komikNgPlaystore + "%2Fdocs%2F&locale=en_US&mobile_iframe=false&next=http%3A%2F%2Fstaticxx.facebook.com%2Fconnect%2Fxd_arbiter%2Fr%2FXBwzv5Yrm_1.js%3Fversion%3D42%23cb%3Df2d150f2c37f0fc%26domain%3Dwww.fbrell.com%26origin%3Dhttp%253A%252F%252Fwww.fbrell.com%252Ffbab276df0d66c%26relation%3Dopener%26frame%3Dfe297e5d39d904%26result%3D%2522xxRESULTTOKENxx%2522&quote=" + TextShare(score) + "&sdk=joey", "https://twitter.com/home?status=" + TextShare(score) + "  "+ suncoURL };
        Application.OpenURL(linkURL[indexSocmed]);
    }

    private string TextShare(int scoreUser)
    {
        string teksShare = "Scoreku adalah " + scoreUser +" Ayo mainkan game puk-puk ";
        return teksShare;
    }

    public void SaveAndShare()
    {
        StartCoroutine(SaveGallery());
    }

    private IEnumerator SaveGallery()
    {
        yield return null;
        yield return new WaitForEndOfFrame();
        Texture2D screenshoot = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, false);
        screenshoot.ReadPixels(new Rect(0, 0, Screen.width, Screen.height), 0, 0);
        screenshoot.Apply();
        if (!isAndroid)
        {
            NativeGallery.SaveToGallery(screenshoot, "Sunco Puk-Puk Game", "Score Game.png");
        }
        else
        {
            NativeGallery.SaveToGallery(screenshoot, "Sunco Puk-Puk Game", "Score Game {0}.jpeg");
        }
        isAndroid = !isAndroid;
        panelShareFanpage.SetActive(true);
        //File.WriteAllBytes(Application.persistentDataPath + "/" + "screenshoot.png", screenshoot.EncodeToPNG());
    }

    public void FanpageSunco()
    {
        Application.OpenURL("https://www.facebook.com/SunCoIndonesia/");
    }

}
