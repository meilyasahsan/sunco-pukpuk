﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelIndexManager : MonoBehaviour {

    public GameObject[] centangs;

    public void ChangeDisplayCentang(int index) {
        centangs[index].SetActive(true);
    }
}
