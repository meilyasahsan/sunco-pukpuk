﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelScoreManager : MonoBehaviour {

    public GameObject[] buttonGameover;
    public GameObject loadingText;

    public void HideShowButton(GameObject[] _paramGameobject, bool isActive)
    {
        foreach(GameObject go in _paramGameobject)
        {
            go.SetActive(isActive);
        }
        loadingText.SetActive(!isActive);
    }

    public GameObject[] getButtonGameOver()
    {
        return buttonGameover;
    }
}
